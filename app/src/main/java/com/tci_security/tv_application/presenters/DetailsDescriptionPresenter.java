/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.tci_security.tv_application.presenters;

import android.support.v17.leanback.widget.AbstractDetailsDescriptionPresenter;

import com.tci_security.tv_application.utilities.Camera;

public class DetailsDescriptionPresenter extends AbstractDetailsDescriptionPresenter {

    @Override
    protected void onBindDescription(ViewHolder viewHolder, Object item) {
        Camera camera = (Camera) item;

        if (camera != null) {
            viewHolder.getTitle().setText(camera.getTitle());
            viewHolder.getSubtitle().setText(camera.getCamID());
            viewHolder.getBody().setText(camera.getDescription());
        }
    }
}
