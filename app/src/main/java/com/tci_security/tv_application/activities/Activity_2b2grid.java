package com.tci_security.tv_application.activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.tci_security.tv_application.R;
import com.tci_security.tv_application.activities.fragments.CameraFragment;

import static com.tci_security.tv_application.R.layout.activity_2b2grid;

public class Activity_2b2grid extends Activity implements CameraFragment.OnFragmentInteractionListener {

    private CameraFragment fragment1;
    private CameraFragment fragment2;
    private CameraFragment fragment3;
    private CameraFragment fragment4;

    private int[] cameraID = new int[4];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(activity_2b2grid);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Bundle b = getIntent().getExtras();
        cameraID[0] = b.getInt("camera1");
        cameraID[1] = b.getInt("camera2");
        cameraID[2] = b.getInt("camera3");
        cameraID[3] = b.getInt("camera4");

        fragment1 = CameraFragment.newInstance(cameraID[0]);
        fragment2 = CameraFragment.newInstance(cameraID[1]);
        fragment3 = CameraFragment.newInstance(cameraID[2]);
        fragment4 = CameraFragment.newInstance(cameraID[3]);

        fragmentTransaction.add(R.id.fragment1, fragment1);
        fragmentTransaction.add(R.id.fragment2, fragment2);
        fragmentTransaction.add(R.id.fragment3, fragment3);
        fragmentTransaction.add(R.id.fragment4, fragment4);

        fragmentTransaction.commit();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d("CDA", "onBackPressed Called");
        // Remove callback on each fragment
        fragment1.onBackPressed();
        fragment2.onBackPressed();
        fragment3.onBackPressed();
        fragment4.onBackPressed();
    }
}
