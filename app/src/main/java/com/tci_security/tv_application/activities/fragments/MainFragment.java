/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.tci_security.tv_application.activities.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v17.leanback.app.BackgroundManager;
import android.support.v17.leanback.app.BrowseFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.OnItemViewClickedListener;
import android.support.v17.leanback.widget.OnItemViewSelectedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowPresenter;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.tci_security.tv_application.R;
import com.tci_security.tv_application.application.TVApp;
import com.tci_security.tv_application.activities.Activity_2b2grid;
import com.tci_security.tv_application.activities.Activity_4b4grid;
import com.tci_security.tv_application.activities.BrowseErrorActivity;
import com.tci_security.tv_application.activities.DetailsActivity;
import com.tci_security.tv_application.activities.PlaybackActivity;
import com.tci_security.tv_application.activities.SettingsActivity;
import com.tci_security.tv_application.presenters.CardPresenter;
import com.tci_security.tv_application.presenters.DigitalClock;
import com.tci_security.tv_application.utilities.Camera;
import com.tci_security.tv_application.utilities.CameraList;
import com.tci_security.tv_application.utilities.URLConverter;
import com.tci_security.tv_application.utilities.Weather;
import com.tci_security.tv_application.utilities.Ping;

import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Arrays;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

public class MainFragment extends BrowseFragment {
    private static final String TAG = "MainFragment";

    private static final int BACKGROUND_UPDATE_DELAY = 300;
    private static final int WEATHER_UPDATE_DELAY = 500; //how long the task waits after
    private static final int WEATHER_UPDATE_PERIOD = 10 * 60 * 1000; //time between weather fetches [10m * 60s * 1000ms]
    private static final int PINGNVR_UPDATE_DELAY = 500; //how long the task waits after
    private static final int PINGNVR_UPDATE_PERIOD = 1 * 60 * 1000; //time between weather fetches [1m * 60s * 1000ms]
    private static final int GRID_ITEM_WIDTH = 200;
    private static final int GRID_ITEM_HEIGHT = 200;
    private static final int NUM_COLS = 16;

    private final Handler mHandler = new Handler();
    private ArrayObjectAdapter mRowsAdapter;
    private Drawable mDefaultBackground;
    private DisplayMetrics mMetrics;
    private Timer mBackgroundTimer;
    private Timer mWeatherTimer;
    private Timer mOnlineStatusTimer;
    private String mBackgroundUri;
    private BackgroundManager mBackgroundManager;

    private SharedPreferences prefs;

    // Weather Widget objects
    private Typeface weatherFont;
    private DigitalClock clock;
    private TextView weatherIcon;
    private TextView temperature;
    private TextView humidity;
    private TextView windSpeed;
    private TextView sunrise_sunset;

    // Ping NVR objects
    private TextView NVRStatusText;
    private ImageView NVRStatusIcon;
    private Ping NVRStatusPing;
    private URL NVRURL;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        //Log.i(TAG, "onCreate");
        super.onActivityCreated(savedInstanceState);

        prepareBackgroundManager();

        setupUIElements();

        loadRows();

        setupEventListeners();

        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        weatherFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/weather-icons.ttf");

        clock = this.getView().findViewById(R.id.clock);
        clock.setAlpha(0.9f);
        weatherIcon = this.getView().findViewById(R.id.weather_icon);
        temperature = this.getView().findViewById(R.id.weather_temperature);
        humidity = this.getView().findViewById(R.id.weather_humidity);
        windSpeed = this.getView().findViewById(R.id.weather_wind);
        sunrise_sunset = this.getView().findViewById(R.id.weather_sunrise_sunset);
        NVRStatusText = this.getView().findViewById(R.id.online_status_text);
        NVRStatusIcon = this.getView().findViewById(R.id.online_status_icon);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != mBackgroundTimer) {
            Log.d(TAG, "onDestroy: " + mBackgroundTimer.toString());
            mBackgroundTimer.cancel();
        }
        if (null != mWeatherTimer) {
            mWeatherTimer.cancel();
        }
        if (null != mOnlineStatusTimer) {
            mOnlineStatusTimer.cancel();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        clock.mCalendar.setTimeZone(TimeZone.getTimeZone(prefs.getString("timezone", getString(R.string.timezone))));
        startBackgroundTimer();
        startWeatherTimer();
        startOnlineStatusTimer();
        toggleClock();
    }

    private void toggleClock() {
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        boolean showClock = prefs.getBoolean("toggleClock", true);

        if (!showClock) {
            if (clock.getVisibility() != View.INVISIBLE) clock.setVisibility(View.INVISIBLE);
        } else {
            if (clock.getVisibility() != View.VISIBLE) clock.setVisibility(View.VISIBLE);
        }
    }

    private void loadRows() {
        List<Camera> list = CameraList.setupCameras();

        mRowsAdapter = new ArrayObjectAdapter(new ListRowPresenter());
        CardPresenter cardPresenter = new CardPresenter();

        ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(cardPresenter);
        for (int j = 0; j < NUM_COLS; j++) {
            listRowAdapter.add(list.get(j % 16));
        }

        HeaderItem header = new HeaderItem(1, "FULLSCREEN");
        mRowsAdapter.add(new ListRow(header, listRowAdapter));


        HeaderItem grid2Header = new HeaderItem(2, "2 BY 2 GRID");
        GridItemPresenter grid2Presenter = new GridItemPresenter();
        ArrayObjectAdapter grid2RowAdapter = new ArrayObjectAdapter(grid2Presenter);

        grid2RowAdapter.add(getResources().getString(R.string.grid_view_2x2_1));
        grid2RowAdapter.add(getResources().getString(R.string.grid_view_2x2_2));
        grid2RowAdapter.add(getResources().getString(R.string.grid_view_2x2_3));
        grid2RowAdapter.add(getResources().getString(R.string.grid_view_2x2_4));

        mRowsAdapter.add(new ListRow(grid2Header, grid2RowAdapter));

        HeaderItem grid4Header = new HeaderItem(3, "VIEW ALL CAMERAS");
        GridItemPresenter grid4Presenter = new GridItemPresenter();
        ArrayObjectAdapter grid4RowAdapter = new ArrayObjectAdapter(grid4Presenter);
        grid4RowAdapter.add(getResources().getString(R.string.grid_view));
        mRowsAdapter.add(new ListRow(grid4Header, grid4RowAdapter));

        HeaderItem preferencesHeader = new HeaderItem(4, "PREFERENCES");
        GridItemPresenter prefPresenter = new GridItemPresenter();
        ArrayObjectAdapter prefRowAdapter = new ArrayObjectAdapter(prefPresenter);
        prefRowAdapter.add(getResources().getString(R.string.personal_settings));
        prefRowAdapter.add(getString(R.string.error_fragment));
        mRowsAdapter.add(new ListRow(preferencesHeader, prefRowAdapter));

        setAdapter(mRowsAdapter);
    }

    private void prepareBackgroundManager() {

        mBackgroundManager = BackgroundManager.getInstance(getActivity());
        mBackgroundManager.attach(getActivity().getWindow());
        mDefaultBackground = getResources().getDrawable(R.drawable.default_background);
        mMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(mMetrics);
    }

    private void setupUIElements() {
        // setBadgeDrawable(getActivity().getResources().getDrawable(
        // R.drawable.videos_by_google_banner));
        setTitle(getString(R.string.browse_title)); // Badge, when set, takes precedent
        // over TITLES
        setHeadersState(HEADERS_ENABLED);
        setHeadersTransitionOnBackEnabled(true);

        // set fastLane (or headers) background color
        setBrandColor(getResources().getColor(R.color.fastlane_background));
        // set search icon color
        setSearchAffordanceColor(getResources().getColor(R.color.search_opaque));
    }

    private void setupEventListeners() {


        setOnItemViewClickedListener(new ItemViewClickedListener());
        setOnItemViewSelectedListener(new ItemViewSelectedListener());
    }

    protected void updateBackground(String uri) {
        int width = mMetrics.widthPixels;
        int height = mMetrics.heightPixels;
        Glide.with(getActivity())
                .load(uri)
                .centerCrop()
                .error(mDefaultBackground)
                .into(new SimpleTarget<GlideDrawable>(width, height) {
                    @Override
                    public void onResourceReady(GlideDrawable resource,
                                                GlideAnimation<? super GlideDrawable>
                                                        glideAnimation) {
                        mBackgroundManager.setDrawable(resource);
                    }
                });
        mBackgroundTimer.cancel();
    }

    private void startBackgroundTimer() {
        if (null != mBackgroundTimer) {
            mBackgroundTimer.cancel();
        }
        mBackgroundTimer = new Timer();
        mBackgroundTimer.schedule(new UpdateBackgroundTask(), BACKGROUND_UPDATE_DELAY);
    }

    private void startWeatherTimer() {
        if (null != mWeatherTimer) mWeatherTimer.cancel();

        mWeatherTimer = new Timer();
        mWeatherTimer.schedule(new UpdateWeatherTask(), WEATHER_UPDATE_DELAY, WEATHER_UPDATE_PERIOD);
    }

    private void startOnlineStatusTimer() {
        if (null != mOnlineStatusTimer) mOnlineStatusTimer.cancel();

        mOnlineStatusTimer = new Timer();
        mOnlineStatusTimer.schedule(new UpdateOnlineStatusTask(), PINGNVR_UPDATE_DELAY, PINGNVR_UPDATE_PERIOD);
    }

    private void updateWeather() {
        new Thread() {
            public void run() {
                Log.d("updateWeather", "getting weather");
                final JSONObject json = Weather.getJSON();
                if (json == null) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(), "Error loading weather", Toast.LENGTH_LONG).show();
                        }
                    });
                }
                else {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            renderWeather(json);
                        }
                    });
                }
            }
        }.start();
    }

    private void updateOnlineStatus() {
        new Thread() {
            public void run() {
                Log.d("updateOnlineStatus", "pinging NVR");
                try {
                    NVRURL = new URL(getString(R.string.nvrurl));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                Ping.ping(NVRURL, TVApp.getAppContext());

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                       renderOnlineStatus();
                    }
                });
            }
        }.start();
    }

    /**
     * Renders the weather from openweathermap into the widget
     *
     * @param json
     */
    private void renderWeather(JSONObject json) {
        try {
            JSONObject weather = json.getJSONArray("weather").getJSONObject(0);
            JSONObject main = json.getJSONObject("main");
            JSONObject wind = json.getJSONObject("wind");
            JSONObject ss = json.getJSONObject("sys");

            prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

            String units = prefs.getString("units_weather", getString(R.string.weather_units));
            String temp_letter, wind_letter;
            Double temp = wind.getDouble("speed");
            if (units.equals("metric")) {
                temp_letter = "C";
                wind_letter = "km/h";
                temp *= 3.6;
            }
            else {
                temp_letter = "F";
                wind_letter = "mph";
            }

            temperature.setText(String.format(Locale.CANADA,"%.1f %s%s", main.getDouble("temp"), "\u00b0", temp_letter));
            humidity.setText(String.format(Locale.CANADA,"Humidity: %.1f%s", main.getDouble("humidity"), "%"));
            windSpeed.setText(String.format(Locale.CANADA,"Wind: %.1f %s", temp, wind_letter));

            TimeZone tz = TimeZone.getTimeZone(prefs.getString("timezone", getString(R.string.timezone)));
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.CANADA);
            sdf.setTimeZone(tz);

            String sunrise = sdf.format(new Date(ss.getLong("sunrise") * 1000));
            String sunset = sdf.format(new Date(ss.getLong("sunset") * 1000));

            sunrise_sunset.setText(String.format("Sunrise: %s\nSunset: %s", sunrise, sunset));

            if (!prefs.getBoolean("toggleSunset", true))
                sunrise_sunset.setVisibility(View.INVISIBLE);
            else
                sunrise_sunset.setVisibility(View.VISIBLE);

            String icon = Weather.getIconID(weather.getInt("id"), ss.getLong("sunrise") * 1000, ss.getLong("sunset") * 1000);
            weatherIcon.setTypeface(weatherFont);
            weatherIcon.setText(icon);

            toggleWeatherWidget();

        } catch (Exception e) { Log.e("renderWeather", "error rendering weather", e); }
    }

    private void toggleWeatherWidget() {
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        boolean showWeather = prefs.getBoolean("toggleWeather", true);
        boolean showSunrise = prefs.getBoolean("toggleSunset", true);

        if (!showWeather) {
            if (weatherIcon.getVisibility() != View.INVISIBLE) weatherIcon.setVisibility(View.INVISIBLE);
            if (sunrise_sunset.getVisibility() != View.INVISIBLE) sunrise_sunset.setVisibility(View.INVISIBLE);
            if (temperature.getVisibility() != View.INVISIBLE) temperature.setVisibility(View.INVISIBLE);
            if (humidity.getVisibility() != View.INVISIBLE) humidity.setVisibility(View.INVISIBLE);
            if (windSpeed.getVisibility() != View.INVISIBLE) windSpeed.setVisibility(View.INVISIBLE);
        } else {
            if (weatherIcon.getVisibility() != View.VISIBLE) weatherIcon.setVisibility(View.VISIBLE);
            if (sunrise_sunset.getVisibility() != View.VISIBLE && showSunrise) sunrise_sunset.setVisibility(View.VISIBLE);
            if (temperature.getVisibility() != View.VISIBLE) temperature.setVisibility(View.VISIBLE);
            if (humidity.getVisibility() != View.VISIBLE) humidity.setVisibility(View.VISIBLE);
            if (windSpeed.getVisibility() != View.VISIBLE) windSpeed.setVisibility(View.VISIBLE);
        }
    }

    private void renderOnlineStatus() {
        if (Ping.isConnected) {
            NVRStatusIcon.setImageResource(R.drawable.online);
            NVRStatusText.setText(getResources().getString(R.string.nvr_status_online));
        }
        else if (!Ping.isConnected) {
            NVRStatusIcon.setImageResource(R.drawable.offline);
            NVRStatusText.setText(getResources().getString(R.string.nvr_status_offline) + " " + Ping.lastOfflineTime);
        }

        toggleNVRWidget();
    }

    private void toggleNVRWidget() {
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        boolean showNVR = prefs.getBoolean("togglePing", true);

        if (!showNVR) {
            if (NVRStatusIcon.getVisibility() != View.INVISIBLE) NVRStatusIcon.setVisibility(View.INVISIBLE);
            if (NVRStatusText.getVisibility() != View.INVISIBLE) NVRStatusText.setVisibility(View.INVISIBLE);
        } else {
            if (NVRStatusIcon.getVisibility() != View.VISIBLE) NVRStatusIcon.setVisibility(View.VISIBLE);
            if (NVRStatusText.getVisibility() != View.VISIBLE) NVRStatusText.setVisibility(View.VISIBLE);
        }
    }

    private final class ItemViewClickedListener implements OnItemViewClickedListener {
        @Override
        public void onItemClicked(Presenter.ViewHolder itemViewHolder, Object item,
                                  RowPresenter.ViewHolder rowViewHolder, Row row) {

            if (item instanceof Camera) {
                Camera movie = (Camera) item;
                ((Camera) item).setVideoUrl(URLConverter.getURL((int)((Camera) item).getId()+1));

                Log.d(TAG, "Item: " + item.toString());
                Intent intent = new Intent(getActivity(), PlaybackActivity.class);
                intent.putExtra(DetailsActivity.MOVIE, movie);

                Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        getActivity(),
                        ((ImageCardView) itemViewHolder.view).getMainImageView(),
                        DetailsActivity.SHARED_ELEMENT_NAME).toBundle();
                getActivity().startActivity(intent, bundle);
            } else if (item instanceof String) {
                if (((String) item).contains(getString(R.string.error_fragment))) {
                    Intent intent = new Intent(getActivity(), BrowseErrorActivity.class);
                    startActivity(intent);
                }

                if (((String) item).contains(getResources().getString(R.string.grid_view_2x2_1))) {
                    Intent intent = new Intent(getActivity(), Activity_2b2grid.class);

                    prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

                    //get array of strings
                    String[] values;
                    try {
                        values = prefs.getStringSet("gridview_1", null).toArray(new String[prefs.getStringSet("gridview_1", null).size()]);
                    } catch (NullPointerException e) {
                        values = new String[]{"1", "2", "3", "4"};
                    }
                    String[] intent_name = {"camera1","camera2","camera3","camera4"};

                    //convert strings to integers and sort them
                    int[] ints = new int[values.length];
                    for (int i = 0; i< values.length; i++) ints[i] = Integer.parseInt(values[i]);
                    Arrays.sort(ints);

                    //put cameras into gridview
                    for (int i = 0; i < values.length && i < 4; i++) {
                        Log.d("gridview1", intent_name[i] + " && " + ints[i]);
                        intent.putExtra(intent_name[i], ints[i]);
                    }

                    startActivity(intent);
                }

                if (((String) item).contains(getResources().getString(R.string.grid_view_2x2_2))) {
                    Intent intent = new Intent(getActivity(), Activity_2b2grid.class);

                    prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

                    //get array of strings
                    String[] values;
                    try {
                        values = prefs.getStringSet("gridview_2", null).toArray(new String[prefs.getStringSet("gridview_2", null).size()]);
                    } catch (NullPointerException e) {
                        values = new String[]{"5","6","7","8"};
                    }
                    String[] intent_name = {"camera1","camera2","camera3","camera4"};

                    //convert strings to integers and sort them
                    int[] ints = new int[values.length];
                    for (int i = 0; i< values.length; i++) ints[i] = Integer.parseInt(values[i]);
                    Arrays.sort(ints);

                    //put cameras into gridview
                    for (int i = 0; i < values.length && i < 4; i++) {
                        Log.d("gridview2", intent_name[i] + " && " + ints[i]);
                        intent.putExtra(intent_name[i], ints[i]);
                    }

                    startActivity(intent);
                }

                if (((String) item).contains(getResources().getString(R.string.grid_view_2x2_3))) {
                    Intent intent = new Intent(getActivity(), Activity_2b2grid.class);

                    prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

                    //get array of strings
                    String[] values;
                    try {
                        values = prefs.getStringSet("gridview_3", null).toArray(new String[prefs.getStringSet("gridview_3",null).size()]);
                    } catch (NullPointerException e) {
                        values = new String[]{"9", "10", "11", "12"};
                    }
                    String[] intent_name = {"camera1","camera2","camera3","camera4"};

                    //convert strings to integers and sort them
                    int[] ints = new int[values.length];
                    for (int i = 0; i< values.length; i++) ints[i] = Integer.parseInt(values[i]);
                    Arrays.sort(ints);

                    //put cameras into gridview
                    for (int i = 0; i < values.length && i < 4; i++) {
                        Log.d("gridview3", intent_name[i] + " && " + ints[i]);
                        intent.putExtra(intent_name[i], ints[i]);
                    }

                    startActivity(intent);
                }

                if (((String) item).contains(getResources().getString(R.string.grid_view_2x2_4))) {
                    Intent intent = new Intent(getActivity(), Activity_2b2grid.class);

                    prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

                    //get array of strings
                    String[] values;
                    try {
                        values = prefs.getStringSet("gridview_4", null).toArray(new String[prefs.getStringSet("gridview_4",null).size()]);
                    } catch (NullPointerException e) {
                        values = new String[]{"13", "14", "15", "16"};
                    }
                    String[] intent_name = {"camera1","camera2","camera3","camera4"};

                    //convert strings to integers and sort them
                    int[] ints = new int[values.length];
                    for (int i = 0; i< values.length; i++) ints[i] = Integer.parseInt(values[i]);
                    Arrays.sort(ints);

                    //put cameras into gridview
                    for (int i = 0; i < values.length && i < 4; i++) {
                        Log.d("gridview4", intent_name[i] + " && " + ints[i]);
                        intent.putExtra(intent_name[i], ints[i]);
                    }

                    startActivity(intent);
                }
                if (((String) item).contains(getResources().getString(R.string.grid_view))) {
                    Intent intent = new Intent(getActivity(), Activity_4b4grid.class);
                    startActivity(intent);
                }
                if (((String) item).contains(getResources().getString(R.string.personal_settings))) {
                    Intent intent = new Intent(getActivity(), SettingsActivity.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(getActivity(), ((String) item), Toast.LENGTH_SHORT)
                            .show();
                }
            }
        }
    }

    private final class ItemViewSelectedListener implements OnItemViewSelectedListener {
        @Override
        public void onItemSelected(Presenter.ViewHolder itemViewHolder, Object item,
                                   RowPresenter.ViewHolder rowViewHolder, Row row) {

            if (item instanceof Camera) {
                mBackgroundUri = ((Camera) item).getBackgroundImageUrl();
                startBackgroundTimer();
            }
        }
    }

    private class UpdateBackgroundTask extends TimerTask {

        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    updateBackground(mBackgroundUri);
                }
            });
        }
    }

    private class UpdateWeatherTask extends TimerTask {
        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Log.d("UpdateWeatherTask", "updating weather");
                    updateWeather();
                }
            });
        }
    }

    private class UpdateOnlineStatusTask extends TimerTask {
        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Log.d("UpdateOnlineStatusTask", "updating NVR status");
                    updateOnlineStatus();
                }
            });
        }
    }

    private int imageIds[] = {
            R.drawable.gridicon2,
            R.drawable.gridicon2,
            R.drawable.gridicon2,
            R.drawable.gridicon2,
            R.drawable.gridicon4,
            R.drawable.settings,
            R.drawable.refreshicon,
    };

    //start at first image
    int imageId = 0;

    private class GridItemPresenter extends Presenter {
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent) {
            TextView view = new TextView(parent.getContext());
            view.setLayoutParams(new ViewGroup.LayoutParams(GRID_ITEM_WIDTH, GRID_ITEM_HEIGHT));
            view.setFocusable(true);
            view.setFocusableInTouchMode(true);
            view.setBackgroundColor(getResources().getColor(R.color.lb_error_background_color_translucent));
            view.setBackground(ContextCompat.getDrawable(TVApp.getAppContext(), imageIds[imageId]));

            //increment imageId
            imageId++;
            view.setTextColor(Color.WHITE);
            view.setGravity(Gravity.CENTER);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, Object item) {
            ((TextView) viewHolder.view).setText((String) item);
        }

        @Override
        public void onUnbindViewHolder(ViewHolder viewHolder) {
        }
    }
}