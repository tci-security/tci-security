package com.tci_security.tv_application.activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.tci_security.tv_application.R;
import com.tci_security.tv_application.activities.fragments.CameraFragment;

import static com.tci_security.tv_application.R.layout.activity_4b4grid;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_1;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_10;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_11;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_12;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_13;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_14;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_15;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_16;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_2;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_3;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_4;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_5;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_6;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_7;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_8;
import static com.tci_security.tv_application.utilities.CameraDefinition.CAMERA_ID_9;

public class Activity_4b4grid extends Activity implements CameraFragment.OnFragmentInteractionListener {

    private CameraFragment fragment1;
    private CameraFragment fragment2;
    private CameraFragment fragment3;
    private CameraFragment fragment4;
    private CameraFragment fragment5;
    private CameraFragment fragment6;
    private CameraFragment fragment7;
    private CameraFragment fragment8;
    private CameraFragment fragment9;
    private CameraFragment fragment10;
    private CameraFragment fragment11;
    private CameraFragment fragment12;
    private CameraFragment fragment13;
    private CameraFragment fragment14;
    private CameraFragment fragment15;
    private CameraFragment fragment16;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(activity_4b4grid);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragment1 = CameraFragment.newInstance(CAMERA_ID_1);
        fragment2 = CameraFragment.newInstance(CAMERA_ID_2);
        fragment3 = CameraFragment.newInstance(CAMERA_ID_3);
        fragment4 = CameraFragment.newInstance(CAMERA_ID_4);
        fragment5 = CameraFragment.newInstance(CAMERA_ID_5);
        fragment6 = CameraFragment.newInstance(CAMERA_ID_6);
        fragment7 = CameraFragment.newInstance(CAMERA_ID_7);
        fragment8 = CameraFragment.newInstance(CAMERA_ID_8);
        fragment9 = CameraFragment.newInstance(CAMERA_ID_9);
        fragment10 = CameraFragment.newInstance(CAMERA_ID_10);
        fragment11 = CameraFragment.newInstance(CAMERA_ID_11);
        fragment12 = CameraFragment.newInstance(CAMERA_ID_12);
        fragment13 = CameraFragment.newInstance(CAMERA_ID_13);
        fragment14 = CameraFragment.newInstance(CAMERA_ID_14);
        fragment15 = CameraFragment.newInstance(CAMERA_ID_15);
        fragment16 = CameraFragment.newInstance(CAMERA_ID_16);

        fragmentTransaction.add(R.id.fragment1, fragment1);
        fragmentTransaction.add(R.id.fragment2, fragment2);
        fragmentTransaction.add(R.id.fragment3, fragment3);
        fragmentTransaction.add(R.id.fragment4, fragment4);
        fragmentTransaction.add(R.id.fragment5, fragment5);
        fragmentTransaction.add(R.id.fragment6, fragment6);
        fragmentTransaction.add(R.id.fragment7, fragment7);
        fragmentTransaction.add(R.id.fragment8, fragment8);
        fragmentTransaction.add(R.id.fragment9, fragment9);
        fragmentTransaction.add(R.id.fragment10, fragment10);
        fragmentTransaction.add(R.id.fragment11, fragment11);
        fragmentTransaction.add(R.id.fragment12, fragment12);
        fragmentTransaction.add(R.id.fragment13, fragment13);
        fragmentTransaction.add(R.id.fragment14, fragment14);
        fragmentTransaction.add(R.id.fragment15, fragment15);
        fragmentTransaction.add(R.id.fragment16, fragment16);

        fragmentTransaction.commit();


    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d("GRID", "onBackPressed Called");

        // Remove callback on each fragment
        fragment1.onBackPressed();
        fragment2.onBackPressed();
        fragment3.onBackPressed();
        fragment4.onBackPressed();
        fragment5.onBackPressed();
        fragment6.onBackPressed();
        fragment7.onBackPressed();
        fragment8.onBackPressed();
        fragment9.onBackPressed();
        fragment10.onBackPressed();
        fragment11.onBackPressed();
        fragment12.onBackPressed();
        fragment13.onBackPressed();
        fragment14.onBackPressed();
        fragment15.onBackPressed();
        fragment16.onBackPressed();
    }

}
