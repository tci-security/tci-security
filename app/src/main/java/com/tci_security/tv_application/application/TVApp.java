package com.tci_security.tv_application.application;

import android.app.Application;
import android.content.Context;

/**
 * Created by glenjeffreympoyonkali on 2018-02-02.
 * Class used to get the application context from wherever in the app to remove all dependencies to
 * activities contexts
 */

public class TVApp extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
        TVApp.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return TVApp.context;
    }
}
