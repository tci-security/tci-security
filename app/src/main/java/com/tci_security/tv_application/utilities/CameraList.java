/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.tci_security.tv_application.utilities;

import android.content.Context;

import com.tci_security.tv_application.R;
import com.tci_security.tv_application.application.TVApp;

import java.util.ArrayList;
import java.util.List;

public final class CameraList {

    //can't use Resource.getSystem() to access the string properties because of Robolectric not being able to
    //figure out the path to those properties without a specified context
    private static Context context = TVApp.getAppContext();
    public static String DESCRIPTION = context.getResources().getString(R.string.camera_description);

    public static String TITLES[] = {
            context.getResources().getString(R.string.camera_name_1),
            context.getResources().getString(R.string.camera_name_2),
            context.getResources().getString(R.string.camera_name_3),
            context.getResources().getString(R.string.camera_name_4),
            context.getResources().getString(R.string.camera_name_5),
            context.getResources().getString(R.string.camera_name_6),
            context.getResources().getString(R.string.camera_name_7),
            context.getResources().getString(R.string.camera_name_8),
            context.getResources().getString(R.string.camera_name_9),
            context.getResources().getString(R.string.camera_name_10),
            context.getResources().getString(R.string.camera_name_11),
            context.getResources().getString(R.string.camera_name_12),
            context.getResources().getString(R.string.camera_name_13),
            context.getResources().getString(R.string.camera_name_14),
            context.getResources().getString(R.string.camera_name_15),
            context.getResources().getString(R.string.camera_name_16)
    };

    public static String CAM_IDS[] = {
            context.getResources().getString(R.string.camera_id_1),
            context.getResources().getString(R.string.camera_id_2),
            context.getResources().getString(R.string.camera_id_3),
            context.getResources().getString(R.string.camera_id_4),
            context.getResources().getString(R.string.camera_id_5),
            context.getResources().getString(R.string.camera_id_6),
            context.getResources().getString(R.string.camera_id_7),
            context.getResources().getString(R.string.camera_id_8),
            context.getResources().getString(R.string.camera_id_9),
            context.getResources().getString(R.string.camera_id_10),
            context.getResources().getString(R.string.camera_id_11),
            context.getResources().getString(R.string.camera_id_12),
            context.getResources().getString(R.string.camera_id_13),
            context.getResources().getString(R.string.camera_id_14),
            context.getResources().getString(R.string.camera_id_15),
            context.getResources().getString(R.string.camera_id_16)
    };

    public static String BG_IMG_URLS[] = {
            context.getResources().getString(R.string.camera_bg_img_url_1),
            context.getResources().getString(R.string.camera_bg_img_url_2),
            context.getResources().getString(R.string.camera_bg_img_url_3),
            context.getResources().getString(R.string.camera_bg_img_url_4),
            context.getResources().getString(R.string.camera_bg_img_url_5),
            context.getResources().getString(R.string.camera_bg_img_url_6),
            context.getResources().getString(R.string.camera_bg_img_url_7),
            context.getResources().getString(R.string.camera_bg_img_url_8),
            context.getResources().getString(R.string.camera_bg_img_url_9),
            context.getResources().getString(R.string.camera_bg_img_url_10),
            context.getResources().getString(R.string.camera_bg_img_url_11),
            context.getResources().getString(R.string.camera_bg_img_url_12),
            context.getResources().getString(R.string.camera_bg_img_url_13),
            context.getResources().getString(R.string.camera_bg_img_url_14),
            context.getResources().getString(R.string.camera_bg_img_url_15),
            context.getResources().getString(R.string.camera_bg_img_url_16)
    };

    public static String CARD_IMG_URLS[] = {
            context.getResources().getString(R.string.camera_card_img_url_1),
            context.getResources().getString(R.string.camera_card_img_url_2),
            context.getResources().getString(R.string.camera_card_img_url_3),
            context.getResources().getString(R.string.camera_card_img_url_4),
            context.getResources().getString(R.string.camera_card_img_url_5),
            context.getResources().getString(R.string.camera_card_img_url_6),
            context.getResources().getString(R.string.camera_card_img_url_7),
            context.getResources().getString(R.string.camera_card_img_url_8),
            context.getResources().getString(R.string.camera_card_img_url_9),
            context.getResources().getString(R.string.camera_card_img_url_10),
            context.getResources().getString(R.string.camera_card_img_url_11),
            context.getResources().getString(R.string.camera_card_img_url_12),
            context.getResources().getString(R.string.camera_card_img_url_13),
            context.getResources().getString(R.string.camera_card_img_url_14),
            context.getResources().getString(R.string.camera_card_img_url_15),
            context.getResources().getString(R.string.camera_card_img_url_16)
    };

    private static List<Camera> list;
    private static long count = 0;

    public static List<Camera> getList() {
        if (list == null) {
            list = setupCameras();
        }
        return list;
    }

    public static List<Camera> setupCameras() {
        list = new ArrayList<>();

        String videoUrl[] = new String[16];
        for (int i = 0; i < 16; i++)
            videoUrl[i] = URLConverter.getURL(i+1);

        for (int index = 0; index < TITLES.length; ++index) {
            list.add(
                    buildCameraInfo(TITLES[index],
                            DESCRIPTION,
                            CAM_IDS[index],
                            videoUrl[index],
                            CARD_IMG_URLS[index],
                            BG_IMG_URLS[index]));
        }

        count = 0;
        return list;
    }

    private static Camera buildCameraInfo(String title,
                                        String description, String camID, String videoUrl, String cardImageUrl,
                                        String backgroundImageUrl) {
        Camera camera = new Camera();
        camera.setId(count++);
        camera.setTitle(title);
        camera.setDescription(description);
        camera.setCamID(camID);
        camera.setCardImageUrl(cardImageUrl);
        camera.setBackgroundImageUrl(backgroundImageUrl);
        camera.setVideoUrl(videoUrl);
        return camera;
    }
}