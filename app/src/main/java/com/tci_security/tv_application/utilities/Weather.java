package com.tci_security.tv_application.utilities;

import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;

import com.tci_security.tv_application.R;
import com.tci_security.tv_application.application.TVApp;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

/**
 * Created by Connor Vacheresse on 2018-01-17.
 *
 * Taken from https://code.tutsplus.com/tutorials/create-a-weather-app-on-android--cms-21587
 */

public class Weather {
    private static final String API_QUERY = "http://api.openweathermap.org/data/2.5/weather?id=%s&units=%s";
    private static Context context = TVApp.getAppContext();

    /**
     * Weather utility that fetches weather data from http://openweathermap.org
     *
     * Takes cityid, apikey, and unit from resource files.
     *
     * @return JSONObject   JSONObject containing weather data
     */
    public static JSONObject getJSON() {
        try {
            String id, units;
            id = context.getResources().getString(R.string.weather_city_id);

            units = PreferenceManager.getDefaultSharedPreferences(context).getString("units_weather", context.getResources().getString(R.string.weather_units));

            URL url = new URL(String.format(API_QUERY, id, units));
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();

            connection.addRequestProperty("x-api-key", context.getResources().getString(R.string.weather_api));

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuffer json = new StringBuffer(1024);

            String tmp = "";
            while ((tmp=br.readLine()) != null) {
                json.append(tmp).append("\n");
            }

            br.close();

            JSONObject result = new JSONObject(json.toString());
            if (result.getInt("cod") != 200)
                return null;

            return result;

        } catch (Exception e) {
            Log.e("getJSON", "Error", e);
            return null;
        }
    }

    /**
     * Takes a weathers code from openweathermap.org and converts it into
     * a String containing the proper weather icon.
     *
     * @param weatherCode
     * @param sunrise
     * @param sunset
     * @return String
     */
    public static String getIconID(int weatherCode, long sunrise, long sunset) {
        String icon = "";
        long now = new Date().getTime();

        //First check all special cases
        switch (weatherCode) {
            case 902: //hurricane
                icon = context.getResources().getString(R.string.weather_hurricane);
                break;
            case 904: //extreme heat
                icon = context.getResources().getString(R.string.weather_hot);
                break;
            case 906: //hail
                icon = context.getResources().getString(R.string.weather_hail);
                break;
            case 800: //clear
                if (now >= sunrise && now <= sunset) //daytime
                    icon = context.getResources().getString(R.string.weather_day_clear);
                else //nighttime
                    icon = context.getResources().getString(R.string.weather_night_clear);
                break;
            case 801: //partially cloudy
                if (now >= sunrise && now <= sunset) //daytime
                    icon = context.getResources().getString(R.string.weather_day_cloudy);
                else //nighttime
                    icon = context.getResources().getString(R.string.weather_night_cloudy);
                break;
            default:
                int code = weatherCode / 100; //only check the 1st number of code
                switch (code) {
                    case 2: //thunder
                        icon = context.getResources().getString(R.string.weather_thunderstorm);
                        break;
                    case 3: //light rain
                        icon = context.getResources().getString(R.string.weather_drizzle);
                        break;
                    case 5: //heavy rain
                        icon = context.getResources().getString(R.string.weather_rain);
                        break;
                    case 6: //snow
                        icon = context.getResources().getString(R.string.weather_snow);
                        break;
                    case 7: //fog/mist
                        icon = context.getResources().getString(R.string.weather_fog);
                        break;
                    case 8: //cloudy
                        icon = context.getResources().getString(R.string.weather_cloudy);
                        break;
                    default: //error
                        icon = context.getResources().getString(R.string.weather_unknown);
                        break;
                }
        }

        return icon;
    }
}
