package com.tci_security.tv_application.utilities;

/**
 * Created by mohamadouly on 2018-02-18.
 *
 * Public API for access cameras
 */

public class CameraDefinition {

    public static final int CAMERA_ID_1 = 1;
    public static final int CAMERA_ID_2 = 2;
    public static final int CAMERA_ID_3 = 3;
    public static final int CAMERA_ID_4 = 4;
    public static final int CAMERA_ID_5 = 5;
    public static final int CAMERA_ID_6 = 6;
    public static final int CAMERA_ID_7 = 7;
    public static final int CAMERA_ID_8 = 8;
    public static final int CAMERA_ID_9 = 9;
    public static final int CAMERA_ID_10 = 10;
    public static final int CAMERA_ID_11 = 11;
    public static final int CAMERA_ID_12 = 12;
    public static final int CAMERA_ID_13 = 13;
    public static final int CAMERA_ID_14 = 14;
    public static final int CAMERA_ID_15 = 15;
    public static final int CAMERA_ID_16 = 16;

    public static final int NUM_CAMERA = 16;


}
