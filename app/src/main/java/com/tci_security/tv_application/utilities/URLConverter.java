package com.tci_security.tv_application.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.tci_security.tv_application.R;
import com.tci_security.tv_application.application.TVApp;

/**
 * Created by Connor Vacheresse on 2018-01-15.
 */

public class URLConverter {

    private static Context context = TVApp.getAppContext();
    public static final String MAINSTREAM = "0";
    public static final String SUBSTREAM = "1";

    public static final boolean LOCAL = false;
    public static final boolean REMOTE = true;
    public static final boolean MODE = REMOTE;


    public static String getURL(int cameraID) {
        return getURL(cameraID, false);
    }

    /**
     * Builds a String that contains an URL for a camera stream.
     *
     * @param cameraID       the camera that you want to access
     * @param forceSubStream force substream mode (for grid views)
     * @return               URL stored in a String
     */
    public static String getURL(int cameraID, boolean forceSubStream) {
        String url = "";

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);

        boolean hostMode = pref.getBoolean("local_remote", MODE);

        String quality = pref.getString("stream_quality", SUBSTREAM);
        String username = pref.getString("username", context.getResources().getString(R.string.camera_username));
        String password = pref.getString("password", context.getResources().getString(R.string.camera_password));

        String host, port;

        if (hostMode == REMOTE) {
            port = pref.getString("port_remote", context.getResources().getString(R.string.camera_remote_port));
            host = pref.getString("host_remote", context.getResources().getString(R.string.camera_remote_host));
        } else {
            port = pref.getString("port_local", context.getResources().getString(R.string.camera_local_port));
            host = pref.getString("host_local", context.getResources().getString(R.string.camera_local_host));
        }

        if (forceSubStream) quality = SUBSTREAM;

        //build the string
        url = "rtsp://" + username + ":" + password + "@" + host + ":" + port + "/unicast/c" + cameraID + "/s" + quality + "/live";
        Log.d("URLConverter", url);

        return url;
    }
}
