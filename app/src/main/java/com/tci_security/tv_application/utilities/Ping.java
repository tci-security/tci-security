package com.tci_security.tv_application.utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Ping {
    public String net = "NO_CONNECTION";
    public String host;
    public String ip;
    public int dns = Integer.MAX_VALUE;
    public int cnt = Integer.MAX_VALUE;
    public static boolean isConnected = false;
    private static final String TAG = "Ping";
    public static String lastOfflineTime = null;

    public static void ping(URL url, Context ctx) {
        Ping r = new Ping();
        if (isNetworkConnected(ctx)) {
            r.net = getNetworkType(ctx);
            try {
                String hostAddress;
                long start = System.currentTimeMillis();
                hostAddress = InetAddress.getByName(url.getHost()).getHostAddress();
                long dnsResolved = System.currentTimeMillis();
                Socket socket = new Socket(hostAddress, url.getPort());
                socket.close();
                long probeFinish = System.currentTimeMillis();
                r.dns = (int) (dnsResolved - start);
                r.cnt = (int) (probeFinish - dnsResolved);
                r.host = url.getHost();
                r.ip = hostAddress;
                r.isConnected = true;
                r.lastOfflineTime = null; //reset timestamp on connection
                Log.d(TAG, "ping successfull: " + r.net + " " + r.host + " " + r.ip + ":" + url.getPort() + " " + r.isConnected);
            } catch (Exception ex) {
                r.net = "NO_CONNECTION";
                r.isConnected = false;
                //only update timestamp on first offline detection
                if (r.lastOfflineTime == null) {
                    r.lastOfflineTime = getCurrentTimeStamp();
                }
                Log.d(TAG, "Unable to ping " + r.net + " " + r.isConnected + " Since: " + r.lastOfflineTime);
            }
        }
    }

    public static String getCurrentTimeStamp() {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd HH:mm:ss", Locale.CANADA);
            return dateFormat.format(new Date()); // Find todays date
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static String getNetworkType(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            return activeNetwork.getTypeName();
        }
        return null;
    }
}