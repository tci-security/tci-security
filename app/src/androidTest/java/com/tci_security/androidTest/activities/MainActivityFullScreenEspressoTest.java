package com.tci_security.androidTest.activities;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.tci_security.tv_application.R;
import com.tci_security.tv_application.activities.MainActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityFullScreenEspressoTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void mainActivityFullScreenExpressoTest() {
        ViewInteraction rowHeaderView = onView(
                allOf(withId(R.id.row_header), withText("FULLSCREEN"),
                        childAtPosition(
                                childAtPosition(
                                        allOf(withId(R.id.browse_headers), withContentDescription("Navigation menu")),
                                        0),
                                0),
                        isDisplayed()));
        rowHeaderView.perform(click());


        ViewInteraction textView7 = onView(
                allOf(withId(R.id.row_header), withText("FULLSCREEN"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.lb_row_container_header_dock),
                                        0),
                                0),
                        isDisplayed()));
        textView7.check(matches(withText("FULLSCREEN")));

        ViewInteraction linearLayout = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.container_list),
                                0),
                        1),
                        isDisplayed()));
        linearLayout.check(matches(isDisplayed()));

        ViewInteraction frameLayout = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.row_content), withContentDescription("FULLSCREEN"),
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        0)),
                        0),
                        isDisplayed()));
        frameLayout.check(matches(isDisplayed()));


        ViewInteraction textView8 = onView(
                allOf(withId(R.id.title_text), withText("1 Front Yard Kitchen"),
                        childAtPosition(
                                allOf(withId(R.id.info_field),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.FrameLayout.class),
                                                1)),
                                0),
                        isDisplayed()));
        textView8.check(matches(withText("1 Front Yard Kitchen")));

        ViewInteraction textView9 = onView(
                allOf(withId(R.id.content_text), withText("Camera 1"),
                        childAtPosition(
                                allOf(withId(R.id.info_field),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.FrameLayout.class),
                                                1)),
                                1),
                        isDisplayed()));
        textView9.check(matches(withText("Camera 1")));

        ViewInteraction frameLayout2 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.row_content), withContentDescription("FULLSCREEN"),
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        0)),
                        1),
                        isDisplayed()));
        frameLayout2.check(matches(isDisplayed()));


        ViewInteraction textView10 = onView(
                allOf(withId(R.id.title_text), withText("2 Side Master"),
                        childAtPosition(
                                allOf(withId(R.id.info_field),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.FrameLayout.class),
                                                1)),
                                0),
                        isDisplayed()));
        textView10.check(matches(withText("2 Side Master")));

        ViewInteraction textView11 = onView(
                allOf(withId(R.id.content_text), withText("Camera 2"),
                        childAtPosition(
                                allOf(withId(R.id.info_field),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.FrameLayout.class),
                                                1)),
                                1),
                        isDisplayed()));
        textView11.check(matches(withText("Camera 2")));

        ViewInteraction frameLayout3 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                allOf(withId(R.id.row_content), withContentDescription("FULLSCREEN")),
                                2),
                        0),
                        isDisplayed()));
        frameLayout3.check(matches(isDisplayed()));

        ViewInteraction textView12 = onView(
                allOf(withId(R.id.title_text), withText("3 Back Master"),
                        childAtPosition(
                                allOf(withId(R.id.info_field),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.FrameLayout.class),
                                                1)),
                                0),
                        isDisplayed()));
        textView12.check(matches(withText("3 Back Master")));

        ViewInteraction textView13 = onView(
                allOf(withId(R.id.content_text), withText("Camera 3"),
                        childAtPosition(
                                allOf(withId(R.id.info_field),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.FrameLayout.class),
                                                1)),
                                1),
                        isDisplayed()));
        textView13.check(matches(withText("Camera 3")));

        ViewInteraction frameLayout4 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                allOf(withId(R.id.row_content), withContentDescription("FULLSCREEN")),
                                3),
                        0),
                        isDisplayed()));
        frameLayout4.check(matches(isDisplayed()));

        ViewInteraction textView14 = onView(
                allOf(withId(R.id.title_text), withText("4 Master Pool"),
                        childAtPosition(
                                allOf(withId(R.id.info_field),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.FrameLayout.class),
                                                1)),
                                0),
                        isDisplayed()));
        textView14.check(matches(withText("4 Master Pool")));

        ViewInteraction textView15 = onView(
                allOf(withId(R.id.content_text), withText("Camera 4"),
                        childAtPosition(
                                allOf(withId(R.id.info_field),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.FrameLayout.class),
                                                1)),
                                1),
                        isDisplayed()));
        textView15.check(matches(withText("Camera 4")));

        ViewInteraction frameLayout5 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                allOf(withId(R.id.row_content), withContentDescription("FULLSCREEN")),
                                4),
                        0),
                        isDisplayed()));
        frameLayout5.check(matches(isDisplayed()));


        ViewInteraction textView16 = onView(
                allOf(withId(R.id.title_text), withText("5 Guest Bedroom Back"),
                        childAtPosition(
                                allOf(withId(R.id.info_field),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.FrameLayout.class),
                                                1)),
                                0),
                        isDisplayed()));
        textView16.check(matches(withText("5 Guest Bedroom Back")));

        ViewInteraction frameLayout6 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                allOf(withId(R.id.row_content), withContentDescription("FULLSCREEN")),
                                5),
                        0),
                        isDisplayed()));
        frameLayout6.check(matches(isDisplayed()));


        ViewInteraction textView17 = onView(
                allOf(withId(R.id.title_text), withText("6 Guest Bedroom Side"),
                        childAtPosition(
                                allOf(withId(R.id.info_field),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.FrameLayout.class),
                                                1)),
                                0),
                        isDisplayed()));
        textView17.check(matches(withText("6 Guest Bedroom Side")));

        ViewInteraction textView18 = onView(
                allOf(withId(R.id.content_text), withText("Camera 6"),
                        childAtPosition(
                                allOf(withId(R.id.info_field),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.FrameLayout.class),
                                                1)),
                                1),
                        isDisplayed()));
        textView18.check(matches(withText("Camera 6")));


        ViewInteraction textView19 = onView(
                allOf(withId(R.id.row_header), withText("2 BY 2 GRID"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.lb_row_container_header_dock),
                                        0),
                                0),
                        isDisplayed()));
        textView19.check(matches(withText("2 BY 2 GRID")));

        ViewInteraction linearLayout3 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.container_list),
                                1),
                        1),
                        isDisplayed()));
        linearLayout3.check(matches(isDisplayed()));

        ViewInteraction frameLayout7 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.row_content), withContentDescription("2 BY 2 GRID"),
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        0)),
                        0),
                        isDisplayed()));
        frameLayout7.check(matches(isDisplayed()));

        ViewInteraction textView20 = onView(
                allOf(withText("Gridview 1"),
                        childAtPosition(
                                childAtPosition(
                                        allOf(withId(R.id.row_content), withContentDescription("2 BY 2 GRID")),
                                        0),
                                0),
                        isDisplayed()));
        textView20.check(matches(withText("Gridview 1")));

        ViewInteraction frameLayout8 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.row_content), withContentDescription("2 BY 2 GRID"),
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        0)),
                        1),
                        isDisplayed()));
        frameLayout8.check(matches(isDisplayed()));

        ViewInteraction textView21 = onView(
                allOf(withText("Gridview 2"),
                        childAtPosition(
                                childAtPosition(
                                        allOf(withId(R.id.row_content), withContentDescription("2 BY 2 GRID")),
                                        1),
                                0),
                        isDisplayed()));
        textView21.check(matches(withText("Gridview 2")));

        ViewInteraction frameLayout9 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.row_content), withContentDescription("2 BY 2 GRID"),
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        0)),
                        2),
                        isDisplayed()));
        frameLayout9.check(matches(isDisplayed()));

        ViewInteraction textView22 = onView(
                allOf(withText("Gridview 3"),
                        childAtPosition(
                                childAtPosition(
                                        allOf(withId(R.id.row_content), withContentDescription("2 BY 2 GRID")),
                                        2),
                                0),
                        isDisplayed()));
        textView22.check(matches(withText("Gridview 3")));

        ViewInteraction frameLayout10 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.row_content), withContentDescription("2 BY 2 GRID"),
                                childAtPosition(
                                        IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        0)),
                        3),
                        isDisplayed()));
        frameLayout10.check(matches(isDisplayed()));

        ViewInteraction textView23 = onView(
                allOf(withText("Gridview 4"),
                        childAtPosition(
                                childAtPosition(
                                        allOf(withId(R.id.row_content), withContentDescription("2 BY 2 GRID")),
                                        3),
                                0),
                        isDisplayed()));
        textView23.check(matches(withText("Gridview 4")));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
