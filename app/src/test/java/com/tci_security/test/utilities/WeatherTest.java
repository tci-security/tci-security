package com.tci_security.test.utilities;

import com.tci_security.tv_application.BuildConfig;
import com.tci_security.tv_application.R;
import com.tci_security.tv_application.utilities.Weather;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;

/**
 * Created by glenjeffreympoyonkali on 2018-03-16.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class WeatherTest {
    @Test
    public void getJSONShouldReturnAJSONObject() throws Exception {
        JSONObject json = Weather.getJSON();
        assertNotEquals("The json object should have a coord object", null, json.get("coord"));
        assertNotEquals("The json object should have a weather object", null, json.get("weather"));
        assertNotEquals("The json object should have a base object", null, json.get("base"));
        assertNotEquals("The json object should have a main object", null, json.get("main"));
        assertNotEquals("The json object should have a wind object", null, json.get("wind"));
        assertNotEquals("The json object should have a clouds object", null, json.get("clouds"));
        assertNotEquals("The json object should have a dt object", null, json.get("dt"));
        assertNotEquals("The json object should have a sys object", null, json.get("sys"));
        assertNotEquals("The json object should have a id object", null, json.get("id"));
        assertNotEquals("The json object should have a name object", null, json.get("name"));
        assertNotEquals("The json object should have a cod object", null, json.get("cod"));
    }

    @Test
    public void getIconIDShouldReturnAnIDThatIsPartOTheListOfPossibleIDs() throws Exception {
        JSONObject json = Weather.getJSON();
        JSONObject weather = json.getJSONArray("weather").getJSONObject(0);
        JSONObject ss = json.getJSONObject("sys");
        List iconList = new ArrayList<String>();
        iconList.add( RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_hurricane));
        iconList.add( RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_hot));
        iconList.add( RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_hail));
        iconList.add( RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_day_clear));
        iconList.add( RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_night_clear));
        iconList.add( RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_day_cloudy));
        iconList.add( RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_night_cloudy));
        iconList.add( RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_thunderstorm));
        iconList.add( RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_drizzle));
        iconList.add( RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_rain));
        iconList.add( RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_snow));
        iconList.add( RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_fog));
        iconList.add( RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_cloudy));
        iconList.add( RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_unknown));

        String icon = Weather.getIconID(weather.getInt("id"), ss.getLong("sunrise") * 1000, ss.getLong("sunset") * 1000);
        assertTrue("The Icon ID should be one of the possible options", iconList.contains(icon));
    }

}