package com.tci_security.test.utilities;

import com.tci_security.tv_application.utilities.Camera;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created by glenjeffreympoyonkali on 2018-01-20.
 *
 * Local Unit tests that run on the local JVM
 */
public class CameraTest {

    Camera camera;

    @Before
    public void setUp() throws Exception {
        camera = new Camera();
        camera.setId(1L);
        camera.setDescription("test camera");
        camera.setTitle("test camera");
        camera.setBackgroundImageUrl("background");
        camera.setCardImageUrl("card");
        camera.setVideoUrl("video");
        camera.setCamID("test camera ID");
    }

    @Test
    public void testGetId() throws Exception {
        assertEquals("The camera id must be 1",1L, camera.getId());
    }

    @Test
    public void testSetId() throws Exception {
        Camera cam = new Camera();
        assertEquals("By default the camera id should be 0",0, cam.getId());
        cam.setId(2L);
        assertEquals("The camera id must be 2",2L, cam.getId());
    }

    @Test
    public void testGetTitle() throws Exception {
        assertEquals("The camera title must be: \"test camera\"","test camera", camera.getTitle());
    }

    @Test
    public void testSetTitle() throws Exception {
        Camera cam = new Camera();
        assertEquals("By default the camera title must be null", null, cam.getTitle());
        cam.setTitle("cam TITLES");
        assertEquals("The camera title must be : \"cam TITLES\"","cam TITLES", cam.getTitle());
    }

    @Test
    public void testGetDescription() throws Exception {
        assertEquals("The camera description must be : \"test camera\"", "test camera", camera.getDescription());
    }

    @Test
    public void testSetDescription() throws Exception {
        Camera cam = new Camera();
        assertEquals("By default the camera description must be null", null, cam.getDescription());
        cam.setDescription("cam description");
        assertEquals("The camera description must be : \"cam description\"", "cam description", cam.getDescription());
    }

    @Test
    public void testGetCamID() throws Exception {
        assertEquals("The camera id must be: \"test camera ID\"", "test camera ID", camera.getCamID());
    }

    @Test
    public void testSetCamID() throws Exception {
        Camera cam = new Camera();
        assertEquals("By default the camera id should be null", null, cam.getCamID());
        cam.setCamID("cam CAM_IDS");
        assertEquals("the camera id must be: \"cam CAM_IDS\"", "cam CAM_IDS", cam.getCamID());
    }

    @Test
    public void testGetVideoUrl() throws Exception {
        assertEquals("The camera video URL must be : \"video\"", "video", camera.getVideoUrl());
    }

    @Test
    public void testSetVideoUrl() throws Exception {
        Camera cam = new Camera();
        assertEquals("By default the camera video URL must be null",null, cam.getVideoUrl());
        cam.setVideoUrl("cam video");
        assertEquals("The camera video url must be: \"cam video\"", "cam video", cam.getVideoUrl());
    }

    @Test
    public void testGetBackgroundImageUrl() throws Exception {
        assertEquals("The camera background image url must be: \"background\"", "background", camera.getBackgroundImageUrl());
    }

    @Test
    public void testSetBackgroundImageUrl() throws Exception {
        Camera cam = new Camera();
        assertEquals("By default the camera background image url must be null", null, cam.getBackgroundImageUrl());
        cam.setBackgroundImageUrl("cam background");
        assertEquals("The camera background image url must be : \"cam background\"", "cam background", cam.getBackgroundImageUrl());
    }

    @Test
    public void testGetCardImageUrl() throws Exception {
        assertEquals("The camera card image url must be : \"card\"", "card", camera.getCardImageUrl());
    }

    @Test
    public void testSetCardImageUrl() throws Exception {
        Camera cam = new Camera();
        assertEquals("By default the camera card image url must be null", null, cam.getCardImageUrl());
        cam.setCardImageUrl("cam card");
        assertEquals("The camera card image url must be: \"cam card\"", "cam card", cam.getCardImageUrl());
    }

    @Test
    public void testToString() throws Exception {
        String expected = "Movie{" +
                                "id=" + camera.getId() +
                                ", TITLES='" + camera.getTitle() + '\'' +
                                ", videoUrl='" + camera.getVideoUrl() + '\'' +
                                ", backgroundImageUrl='" + camera.getBackgroundImageUrl() + '\'' +
                                ", cardImageUrl='" + camera.getCardImageUrl() + '\'' +
                            '}';
        assertEquals("The camera string representation matches", expected, camera.toString());

    }

}