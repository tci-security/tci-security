package com.tci_security.test.utilities;

import com.tci_security.tv_application.BuildConfig;
import com.tci_security.tv_application.utilities.Ping;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * Created by Connor on 2018-04-03.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.tci_security.tv_application")
public class PingTest {

    @Test
    public void matchTimeStamp() {
        String timestamp = Ping.getCurrentTimeStamp();
        String now = new SimpleDateFormat("MM-dd HH:mm", Locale.CANADA).format(new Date());

        assertEquals(timestamp.substring(0, timestamp.length()-3), now);
    }

    @Test
    public void testNetworkStatus() {
        try {
            URL google = new URL("google.com");

            Ping.ping(google, RuntimeEnvironment.application.getApplicationContext());
            assertEquals(Ping.isNetworkConnected(RuntimeEnvironment.application.getApplicationContext()), Ping.isConnected);
        } catch (MalformedURLException e) {}
    }

    @Test
    public void testNetworkType() {
        List<String> connectionTypes = new ArrayList<String>();
        connectionTypes.add("Ethernet");
        connectionTypes.add("Wifi");

        try {
            assertNull(Ping.getNetworkType(RuntimeEnvironment.application.getApplicationContext()));
            Ping ping = new Ping();
            ping.ping(new URL("google.com"), RuntimeEnvironment.application.getApplicationContext());

            assertTrue(connectionTypes.contains(ping.net));
        } catch (MalformedURLException e) {}
    }

    @Test
    public void testPingFunction() {
        Ping ping = new Ping();

        try {
            URL google = new URL("google.com");
            URL badURL = new URL("http://asdasdasdasdasdasdas.asdasdasd");

            Ping.ping(google, RuntimeEnvironment.application.getApplicationContext());
            assertTrue(Ping.isConnected);

            Ping.ping(badURL, RuntimeEnvironment.application.getApplicationContext());
            assertFalse(Ping.isConnected);
        } catch (MalformedURLException e) {}
    }
}
