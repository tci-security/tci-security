package com.tci_security.test.utilities;

import com.tci_security.tv_application.BuildConfig;
import com.tci_security.tv_application.utilities.URLConverter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertTrue;

/**
 * Created by glenjeffreympoyonkali on 2018-01-31.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class URLConverterTest {

    private static final String URL_REGEX = "^rtsp:\\/\\/[a-zA-Z0-9!]+:[a-zA-Z0-9!]+@[a-zA-Z0-9.]+:[0-9]{3,4}\\/unicast\\/c1\\/s1\\/live$";

    @Test
    public void getURLWithoutStreamFlagShouldDefaultToFlagAtFalse() throws Exception {
        assertTrue("The URL must match the following regex: \""+URL_REGEX+"\"", URLConverter.getURL(1).matches(URL_REGEX));
    }

    @Test
    public void getURLWithStreamFlagShouldCallTheMethodVersionWith3Arguments() throws Exception {
       assertTrue("The URL must match this regex: \""+URL_REGEX+"\"", URLConverter.getURL(1, true).matches(URL_REGEX));
    }

}