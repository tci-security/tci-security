package com.tci_security.test.utilities;

import com.tci_security.tv_application.BuildConfig;
import com.tci_security.tv_application.utilities.Camera;
import com.tci_security.tv_application.utilities.CameraList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by glenjeffreympoyonkali on 2018-01-31.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, packageName = "com.tci_security.tv_application")
public class CameraListTest {

    private static final String URL_REGEX = "^rtsp:\\/\\/[a-zA-Z0-9!]+:[a-zA-Z0-9!]+@[a-zA-Z0-9.]+:[0-9]{3,4}\\/unicast\\/c[0-9]{1,2}\\/s[0-9]{1}\\/live$";

    @Test
    public void getListShouldTriggerSetupCameraAndReturnAllCameras() throws Exception {
        List<Camera> list = CameraList.getList();
        //the list must have been set, hence should not be null
        assertNotEquals("The camera list must not be null", null, list);

        //now check that the characteristics of all 16 cameras match the expected result
        for(int i = 0; i < 16 ; i++){
            assertEquals("The camera description must be: Difilippo TCI Manor", CameraList.DESCRIPTION, list.get(i).getDescription());
            assertEquals("The camera title must be in the predefined list", CameraList.TITLES[i], list.get(i).getTitle());
            assertEquals("The camera background image url must be in the predefined list", CameraList.BG_IMG_URLS[i], list.get(i).getBackgroundImageUrl());
            assertEquals("The camera id must be in the predefined list", CameraList.CAM_IDS[i], list.get(i).getCamID());
            assertEquals("The camera card image url must be in the predefined list",CameraList.CARD_IMG_URLS[i], list.get(i).getCardImageUrl());
            assertTrue("The camera must have a URL that matches the following regex: \""+URL_REGEX+"\"", list.get(i).getVideoUrl().matches(URL_REGEX));
        }
    }

    @Test
    public void setupCamerasShouldCreate16CameraObjects() throws Exception {
        List<Camera> list =  CameraList.setupCameras();
        //the list object is expected to have 16 camera objects
        assertEquals("There must be 16 cameras",16, list.size());

        //now check that the characteristics of all 16 cameras match the expected result
        for(int i = 0; i < 16 ; i++){
            assertEquals("The camera description must be: Difilippo TCI Manor", CameraList.DESCRIPTION, list.get(i).getDescription());
            assertEquals("The camera title must be in the predefined list", CameraList.TITLES[i], list.get(i).getTitle());
            assertEquals("The camera background image url must be in the predefined list", CameraList.BG_IMG_URLS[i], list.get(i).getBackgroundImageUrl());
            assertEquals("The camera id must be in the predefined list", CameraList.CAM_IDS[i], list.get(i).getCamID());
            assertEquals("The camera card image url must be in the predefined list",CameraList.CARD_IMG_URLS[i], list.get(i).getCardImageUrl());
            assertTrue("The camera must have a URL that matches the following regex: \""+URL_REGEX+"\"", list.get(i).getVideoUrl().matches(URL_REGEX));
        }
    }

}