package com.tci_security.test.activities;

import com.tci_security.tv_application.BuildConfig;
import com.tci_security.tv_application.R;
import com.tci_security.tv_application.activities.PlaybackActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;


/**
 * Created by glenjeffreympoyonkali on 2018-03-16.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class PlaybackActivityTest {

    /**
     * As per the current implementation, a PlaybackActivity cannot be inflated because it needs the activity to which it is attached
     * to be inflated, but when we run this unit test we try to directly instantiate the fragment hence the underlying activity is null
     * so its instantiation should fail
     */
    @Test(expected = NullPointerException.class)
    public void creatingPlaybackActivityShouldThrowANullPointerException() {
        PlaybackActivity activity = Robolectric.buildActivity(PlaybackActivity.class).create().get();
    }

}