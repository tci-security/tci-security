package com.tci_security.test.activities;

import android.view.SurfaceView;
import android.widget.FrameLayout;
import android.widget.TableRow;
import android.widget.VideoView;

import com.tci_security.tv_application.BuildConfig;
import com.tci_security.tv_application.R;
import com.tci_security.tv_application.activities.Activity_2b2grid;
import com.tci_security.tv_application.activities.Activity_4b4grid;
import com.tci_security.tv_application.activities.fragments.CameraFragment;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by glenjeffreympoyonkali on 2018-02-02.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class Activity_4b4gridTest {

    Activity_4b4grid activity;
    @Before
    public void setUp() {
        activity = Robolectric.buildActivity(Activity_4b4grid.class).create().get();
    }

    @Test
    public void testFragment1Loaded() {
        FrameLayout fragment1 = activity.findViewById(R.id.fragment2);
        TableRow.LayoutParams gridSize1 = (TableRow.LayoutParams) fragment1.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize1.height);
        assertNotEquals(0, gridSize1.height);
        assertNotEquals(null, gridSize1.width);
        assertNotEquals(0, gridSize1.width);
    }

    @Test
    public void testFragment2Loaded() {
        FrameLayout fragment2 = activity.findViewById(R.id.fragment2);
        TableRow.LayoutParams gridSize2 = (TableRow.LayoutParams) fragment2.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize2.height);
        assertNotEquals(0, gridSize2.height);
        assertNotEquals(null, gridSize2.width);
        assertNotEquals(0, gridSize2.width);

        //test if the videoview has been started
        //assertEquals(true, camera2.isPlaying());
    }

    @Test
    public void testFragment3Loaded() {


        FrameLayout fragment3 = activity.findViewById(R.id.fragment3);
        TableRow.LayoutParams gridSize3 = (TableRow.LayoutParams) fragment3.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize3.height);
        assertNotEquals(0, gridSize3.height);
        assertNotEquals(null, gridSize3.width);
        assertNotEquals(0, gridSize3.width);
    }
    @Test
    public void testFragment4Loaded() {
        FrameLayout fragment4 = activity.findViewById(R.id.fragment4);
        TableRow.LayoutParams gridSize4 = (TableRow.LayoutParams) fragment4.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize4.height);
        assertNotEquals(0, gridSize4.height);
        assertNotEquals(null, gridSize4.width);
        assertNotEquals(0, gridSize4.width);

    }
    @Test
    public void testFragment5Loaded() {
        FrameLayout fragment5 = activity.findViewById(R.id.fragment5);
        TableRow.LayoutParams gridSize5 = (TableRow.LayoutParams) fragment5.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize5.height);
        assertNotEquals(0, gridSize5.height);
        assertNotEquals(null, gridSize5.width);
        assertNotEquals(0, gridSize5.width);

    }
    @Test
    public void testFragment6Loaded() {
        FrameLayout fragment6 = activity.findViewById(R.id.fragment6);
        TableRow.LayoutParams gridSize6 = (TableRow.LayoutParams) fragment6.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize6.height);
        assertNotEquals(0, gridSize6.height);
        assertNotEquals(null, gridSize6.width);
        assertNotEquals(0, gridSize6.width);
    }

    @Test
    public void testFragment7Loaded() {
        FrameLayout fragment7 = activity.findViewById(R.id.fragment7);
        TableRow.LayoutParams gridSize7 = (TableRow.LayoutParams)fragment7.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize7.height);
        assertNotEquals(0, gridSize7.height);
        assertNotEquals(null, gridSize7.width);
    }

    @Test
    public void testFragment8Loaded() {
        FrameLayout fragment8 = activity.findViewById(R.id.fragment8);
        TableRow.LayoutParams gridSize8 = (TableRow.LayoutParams) fragment8.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize8.height);
        assertNotEquals(0, gridSize8.height);
        assertNotEquals(null, gridSize8.width);
        assertNotEquals(0, gridSize8.width);
    }

    @Test
    public void testFragment9Loaded() {
        FrameLayout fragment9 = activity.findViewById(R.id.fragment9);
        TableRow.LayoutParams gridSize9 = (TableRow.LayoutParams) fragment9.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize9.height);
        assertNotEquals(0, gridSize9.height);
        assertNotEquals(null, gridSize9.width);
        assertNotEquals(0, gridSize9.width);
    }

    @Test
    public void testFragment10Loaded() {
        FrameLayout fragment10 = activity.findViewById(R.id.fragment10);
        TableRow.LayoutParams gridSize10 = (TableRow.LayoutParams) fragment10.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize10.height);
        assertNotEquals(0, gridSize10.height);
        assertNotEquals(null, gridSize10.width);
        assertNotEquals(0, gridSize10.width);
    }
    @Test
    public void testFragment11Loaded() {
        FrameLayout fragment11 = activity.findViewById(R.id.fragment11);
        TableRow.LayoutParams gridSize11 = (TableRow.LayoutParams) fragment11.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize11.height);
        assertNotEquals(0, gridSize11.height);
        assertNotEquals(null, gridSize11.width);
        assertNotEquals(0, gridSize11.width);
    }

    @Test
    public void testFragment12Loaded() {
        FrameLayout fragment12 = activity.findViewById(R.id.fragment12);
        TableRow.LayoutParams gridSize12 = (TableRow.LayoutParams) fragment12.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize12.height);
        assertNotEquals(0, gridSize12.height);
        assertNotEquals(null, gridSize12.width);
        assertNotEquals(0, gridSize12.width);
    }

    @Test
    public void testFragment13Loaded() {
        FrameLayout fragment13 = activity.findViewById(R.id.fragment13);
        TableRow.LayoutParams gridSize13 = (TableRow.LayoutParams) fragment13.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize13.height);
        assertNotEquals(0, gridSize13.height);
        assertNotEquals(null, gridSize13.width);
        assertNotEquals(0, gridSize13.width);
    }

    @Test
    public void testFragment14Loaded() {
        FrameLayout fragment14 = activity.findViewById(R.id.fragment14);
        TableRow.LayoutParams gridSize14 = (TableRow.LayoutParams) fragment14.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize14.height);
        assertNotEquals(0, gridSize14.height);
        assertNotEquals(null, gridSize14.width);
        assertNotEquals(0, gridSize14.width);
    }

    @Test
    public void testFragment15Loaded() {
        FrameLayout fragment15 = activity.findViewById(R.id.fragment15);
        TableRow.LayoutParams gridSize15 = (TableRow.LayoutParams) fragment15.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize15.height);
        assertNotEquals(0, gridSize15.height);
        assertNotEquals(null, gridSize15.width);
        assertNotEquals(0, gridSize15.width);
    }

    @Test
    public void testFragment16Loaded() {
        FrameLayout fragment16 = activity.findViewById(R.id.fragment16);
        TableRow.LayoutParams gridSize16 = (TableRow.LayoutParams)fragment16.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize16.height);
        assertNotEquals(0, gridSize16.height);
        assertNotEquals(null, gridSize16.width);
        assertNotEquals(0, gridSize16.width);
    }

}