package com.tci_security.test.activities;

//import com.tci_security.tv_application.presenters.DigitalClock;
import android.widget.TextView;

import com.tci_security.tv_application.BuildConfig;
import com.tci_security.tv_application.R;
import com.tci_security.tv_application.activities.MainActivity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by glenjeffreympoyonkali on 2018-02-02.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class MainActivityTest {

    List<String> weather_icons;

    @Before
    public void setUp(){
        //as the icon depends on current conditions we will use a list of all possible icons and make sure
        //the one assigned when creating the fragment is from this list
        weather_icons = new ArrayList<String>();
        weather_icons.add(RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_day_clear));
        weather_icons.add(RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_night_clear));
        weather_icons.add(RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_day_cloudy));
        weather_icons.add(RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_night_cloudy));
        weather_icons.add(RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_thunderstorm));
        weather_icons.add(RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_drizzle));
        weather_icons.add(RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_rain));
        weather_icons.add(RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_snow));
        weather_icons.add(RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_fog));
        weather_icons.add(RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_cloudy));
        weather_icons.add(RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_hurricane));
        weather_icons.add(RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_hot));
        weather_icons.add(RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_hail));
        weather_icons.add(RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.weather_unknown));
    }

    @Test
    public void creatingMainActivityShouldLoadAppropriateTextViews() {
        MainActivity activity = Robolectric.buildActivity(MainActivity.class).create().get();

        //DigitalClock clock = activity.findViewById(R.id.clock);
        //assertEquals("The clock Time zone should be America/Puerto_Rico" ,RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.timezone) , clock.getTimeZone());

        //we make sure that the weather icon list has all the icons
        assertEquals("There exist 14 weather icons", 14, weather_icons.size());

        TextView weather_icon = activity.findViewById(R.id.weather_icon);
        assertNotEquals("The weather icon TextView is not null" ,null, weather_icon);
        assertNotEquals("The weather icon TextView text is not null", null, weather_icon.getText());
        assertTrue("The weather icon is part of the list of possible icons" , weather_icons.contains(weather_icon.getText()));

        TextView weather_temperature = activity.findViewById(R.id.weather_temperature);
        assertNotEquals("The weather temperature TextView is not null" ,null, weather_temperature);
        assertNotEquals("The weather temperature TextView text is not null" ,null, weather_temperature.getText());
        assertTrue("The weather temperature is expressed in \u00b0C", weather_temperature.getText().toString().matches("^[0-9]+\\.[0-9]{2}.\\u00b0C$|^$"));

        TextView weather_humidity = activity.findViewById(R.id.weather_humidity);
        assertNotEquals("The weather humidity TextView is not null" , null, weather_humidity);
        assertNotEquals("The weather humidity TextView text is not null" ,null, weather_humidity.getText());
        assertTrue( "The weather humidity is expressed in %", weather_humidity.getText().toString().matches("^Humidity:.[0-9]+\\.[0-9]{2}%$|^$"));

        TextView weather_wind = activity.findViewById(R.id.weather_wind);
        assertNotEquals("The weather wind TextView is not null",null, weather_wind);
        assertNotEquals("The weather wind TextView text is not null",null, weather_wind.getText());
        assertTrue("The weather wind is expressed in km/h", weather_wind.getText().toString().matches("^Wind:.[0-9]+\\.[0-9]{2}.km/h$|^$"));

        TextView weather_sunrise_sunset = activity.findViewById(R.id.weather_sunrise_sunset);
        assertNotEquals("The weather sunrise/sunset TextView is not null",null, weather_sunrise_sunset);
        assertNotEquals("The weather sunrise/sunset TextView text is not null",null, weather_sunrise_sunset.getText());
        assertTrue("The weather sunrise/sunset has this format : Sunrise:*Sunset:*" ,weather_sunrise_sunset.getText().toString().matches("^Sunrise:.[0-9]{2}:[0-9]{2}.[AMP]{2}\\nSunset:.[0-9]{2}:[0-9]{2}.[AMP]{2}$|^$"));
    }

}