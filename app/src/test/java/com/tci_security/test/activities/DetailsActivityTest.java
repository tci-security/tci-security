package com.tci_security.test.activities;

import android.telecom.Call;

import com.tci_security.tv_application.BuildConfig;
import com.tci_security.tv_application.R;
import com.tci_security.tv_application.activities.BrowseErrorActivity;
import com.tci_security.tv_application.activities.DetailsActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

/**
 * Created by glenjeffreympoyonkali on 2018-03-16.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class DetailsActivityTest {

    /**
     * As per the current implementation, a DetailsActivity cannot be inflated because it's just a fragment without any activity
     * so its instantiation should fail.
     */
    @Test(expected = Exception.class)
    public void creatingDetailsActivityShouldThrowAnException() {
        DetailsActivity activity = Robolectric.buildActivity(DetailsActivity.class).create().get();
    }

}