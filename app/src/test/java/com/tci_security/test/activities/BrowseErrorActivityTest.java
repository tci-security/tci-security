package com.tci_security.test.activities;

import com.tci_security.tv_application.BuildConfig;
import com.tci_security.tv_application.R;
import com.tci_security.tv_application.activities.BrowseErrorActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;

/**
 * Created by glenjeffreympoyonkali on 2018-02-02.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class BrowseErrorActivityTest {

    @Test
    public void creatingErrorActivityShouldLoadInAppropriateErrorFragment() {
        BrowseErrorActivity activity = Robolectric.buildActivity(BrowseErrorActivity.class).create().get();
        assertEquals("The Error Fragment title should be TCI SECURITY", RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.app_name), activity.getTitle());

    }

}