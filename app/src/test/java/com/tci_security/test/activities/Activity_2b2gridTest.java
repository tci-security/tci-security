package com.tci_security.test.activities;

import android.content.Intent;
import android.widget.FrameLayout;
import android.widget.TableRow;

import com.tci_security.tv_application.BuildConfig;
import com.tci_security.tv_application.R;
import com.tci_security.tv_application.activities.Activity_2b2grid;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertNotEquals;

/**
 * Created by glenjeffreympoyonkali on 2018-02-02.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class Activity_2b2gridTest {

    private Activity_2b2grid activity;

    @Before
    public void setUp() {
        //we set up the intent necessary to build the activity
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.putExtra("camera1", 1);
        intent.putExtra("camera2", 2);
        intent.putExtra("camera3", 3);
        intent.putExtra("camera4", 4);
        activity = Robolectric.buildActivity(Activity_2b2grid.class, intent).create().get();
    }

    @Test
    public void testFragment1Loaded() {
        FrameLayout fragment1 = activity.findViewById(R.id.fragment1);
        TableRow.LayoutParams gridSize1 = (TableRow.LayoutParams) fragment1.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize1.height);
        assertNotEquals(0, gridSize1.height);
        assertNotEquals(null, gridSize1.width);
        assertNotEquals(0, gridSize1.width);
    }

    @Test
    public void testFragment2Loaded() {
        FrameLayout fragment2 = activity.findViewById(R.id.fragment2);
        TableRow.LayoutParams gridSize2 = (TableRow.LayoutParams) fragment2.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize2.height);
        assertNotEquals(0, gridSize2.height);
        assertNotEquals(null, gridSize2.width);
        assertNotEquals(0, gridSize2.width);
    }

    @Test
    public void testFragment3Loaded() {
        FrameLayout fragment3 = activity.findViewById(R.id.fragment3);
        TableRow.LayoutParams gridSize3 = (TableRow.LayoutParams) fragment3.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize3.height);
        assertNotEquals(0, gridSize3.height);
        assertNotEquals(null, gridSize3.width);
        assertNotEquals(0, gridSize3.width);
    }

    @Test
    public void testFragment4Loaded() {
        FrameLayout fragment4 = activity.findViewById(R.id.fragment4);
        TableRow.LayoutParams gridSize4 = (TableRow.LayoutParams)fragment4.getLayoutParams();
        //the grid element size should not be null and not equal to 0
        assertNotEquals(null, gridSize4.height);
        assertNotEquals(0, gridSize4.height);
        assertNotEquals(null, gridSize4.width);
        assertNotEquals(0, gridSize4.width);

    }
}