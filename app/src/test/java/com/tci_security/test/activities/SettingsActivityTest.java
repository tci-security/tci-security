package com.tci_security.test.activities;

import com.tci_security.tv_application.BuildConfig;
import com.tci_security.tv_application.R;
import com.tci_security.tv_application.activities.SettingsActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

/**
 * Created by glenjeffreympoyonkali on 2018-03-16.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class SettingsActivityTest {

    @Test
    public void creatingSettingsActivityShould() {
        SettingsActivity activity = Robolectric.buildActivity(SettingsActivity.class).create().get();
        assertEquals("The SettingsActivity title should be Settings",RuntimeEnvironment.application.getApplicationContext().getResources().getString(R.string.title_activity_settings), activity.getTitle());

    }

}