#!/bin/bash

#the NDK is not used for the project at this part but the docker image used for pipelines imports it for us
#so we have to unset it to avoid failing builds in pipelines
unset ANDROID_NDK_HOME
# Grab the Android Support Repo which isn't included in the container
echo y | android update sdk --filter "extra-android-m2repository" --no-ui -a
mkdir "${ANDROID_HOME}/licenses" || true
echo "8933bad161af4178b1185d1a37fbf41ea5269c55" > "${ANDROID_HOME}/licenses/android-sdk-license"
echo "d56f5187479451eabf01fb78af6dfcb131a6481e" >> "${ANDROID_HOME}/licenses/android-sdk-license"
echo "84831b9409646a918e30573bab4c9c91346d8abd" > "${ANDROID_HOME}/licenses/android-sdk-preview-license"

./gradlew clean build test jacocoTestReport check
#we need to specify the token when pushing to codecov to identify our repo
bash <(curl -s https://codecov.io/bash) -t 74865263-d7c2-438a-b2b1-c37f88d7c584